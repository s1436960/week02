import java.util.Scanner;

/**
 * Created by s1436960 on 1/30/2017.
 */
public class Main4
{


    public static double convertFahrenheitToCelsius(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature - 32.0) * 5.0 / 9.0;
    }


    public static double convertCelsiusToFahrenheit(double celsiusTemperature)
    {
        double fahrenheit = 0.0;

        fahrenheit = celsiusTemperature * 9.0 / 5.0 + 32.0;

        return fahrenheit;
    }

    public static int convertCelsiusToFahrenheit(int celsiusTemperature)
    {
        int fahrenheit = 0;

        fahrenheit = celsiusTemperature * 9 / 5 + 32;

        return fahrenheit;
    }


    public static void main(String[] args)
    {
        // declare and initialize all variables
        Scanner scannerIn = new Scanner(System.in);
        double fahrenheit = 0.0;
        double celsius = 0.0;
        double kelvin = 0.0;
        int count = 0;
        double temperatureSum = 0.0;
        double temperatureAvg = 0.0;



        // get input
        System.out.print("How many temperatures to process: ");
        count = scannerIn.nextInt();

        for (int i=0; i < count; i ++)
        {
            System.out.print("Enter a fahrenheit temperature: ");
            fahrenheit = scannerIn.nextDouble();


            // do the math
            celsius = convertFahrenheitToCelsius(fahrenheit);
            temperatureSum += celsius;
        }
        temperatureAvg = temperatureSum/ (double)count;




        // display results

        System.out.println("Average celsius temperature is : " + temperatureAvg);


    }
}
