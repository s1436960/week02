import java.util.Scanner;

public class Main
{

    public static void main(String[] args)
    {
        // declare and initialize all variables
        Scanner scannerIn = new Scanner(System.in);
        int     temperature;

        temperature = 42;

        double fahrenheit = 32.0;
        double fahreheitDoubled = 0.0;

        // get input
        System.out.print("Enter a fahrenheit temperature: ");
        fahrenheit = scannerIn.nextDouble();

        // perform calculations
        //fahrenheitDoubled *= 2.0;
        fahreheitDoubled = fahrenheit * 2.0;


        // display output

        // do not use calculations in an output
        // System.out.println("Twice the fahrenheit value is: " + (fahrenheit * 2.0));

        System.out.println("Twice the fahrenheit value is: " + (fahreheitDoubled));

    }
}
