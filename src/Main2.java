import java.util.Scanner;

/**
 * Created by s1436960 on 1/30/2017.
 */
public class Main2
{
      public static void main(String[] args)
      {
          // declare and initialize all variables
          Scanner scannerIn = new Scanner(System.in);
          double a = 0.0;
          double b = 0.0;
          double c = 0.0;
          double x1 = 0.0;
          double x2 = 0.0;

          // get input

          System.out.print("Enter a value for [a]: ");
          a = scannerIn.nextDouble();

          System.out.print("Enter a value for [b]: ");
          b = scannerIn.nextDouble();

          System.out.print("Enter a value for [c]: ");
          c = scannerIn.nextDouble();

          // do the math

          if (a == 0.0)
          {
              System.out.println("Cannot solve the quadratic equation");
          }
          else
          {
              x1 = ( -b + Math.sqrt(b * b - (4.0 * a * c)) ) / (2.0 * a) ;
              x2 = ( -b - Math.sqrt(b * b - (4.0 * a * c)) ) / (2.0 * a) ;
              System.out.println("X1 is: " + x1);
              System.out.println("X2 is: " + x2);
          }


      }
}
